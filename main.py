#!/usr/local/bin/python3

import csv
import numpy as np
from PIL import Image
import pickle
import argparse

parser = argparse.ArgumentParser(description='Visual math.')
parser.add_argument('img', metavar='file', type=str,
                    help='File of image to process')

args = parser.parse_args()

try:
    knn = pickle.load(open("knn.pkl", 'rb'))
except:
    samples = [i for i in csv.reader(open('train.csv', newline=''), delimiter=',', quotechar='|')]
    del(samples[0])
    target = np.array([i[0] for i in samples])
    for sample in samples:
        del(sample[0])
    samples = np.int_(samples)


    from sklearn.neighbors import KNeighborsClassifier

    knn = KNeighborsClassifier(n_neighbors=7)
    knn.fit(samples, target)
    pickle.dump(knn, open("knn.pkl", 'wb'))


img = Image.open(args.img)
img = img.convert('L')
data = np.asarray(np.asarray(img).flat) / 255

print(knn.predict(data.reshape(1, -1))[0])
